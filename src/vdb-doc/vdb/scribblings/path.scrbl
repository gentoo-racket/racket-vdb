;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     vdb/env
                     vdb/path)
          scribble/example
          vdb/path)


@(define example-evaluator
   (make-base-eval '(require vdb/path)))


@title[#:tag "vdb-path"]{Path utilities}


@defmodule[vdb/path]


@defproc[
 (vdb-package-name [category string?]
                   [name     string?]
                   [version  string?])
 string?
 ]{
 Join @racket[category], @racket[name] and @racket[version] in Portage style.

 @examples[
 #:eval example-evaluator
 (vdb-package-name "sys-devel" "gcc" "11.3.0")
 ]
}

@defproc[
 (vdb-package-path [package-full-name string?])
 path?
 ]{
 Return a path to @racket[package-full-name] inside @racket[vdb-path].
}

@defproc[
 (vdb-package-path-join [category string?]
                        [name     string?]
                        [version  string?])
 string?
 ]{
 Return a path to a package inside @racket[vdb-path].
}
