;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     vdb/env
                     vdb/package)
          scribble/example
          vdb/package)


@(define example-evaluator
   (make-base-eval '(require vdb/package)))


@title[#:tag "vdb-package"]{Packages}


@defmodule[vdb/package]


@defproc[
 (path->package-full-name [package-path path-string?])
 string?
 ]{
 Return a Portage style package name from the given @racket[package-path].

 @examples[
 #:eval example-evaluator
 (path->package-full-name "/var/db/pkg/sys-devel/gcc-11.3.0")
 ]
}

@defproc[
 (vdb-packages/path)
 (listof path?)
 ]{
 Return a list of all package paths inside @racket[vdb-path].
}

@defproc[
 (vdb-packages)
 (listof string?)
 ]{
 Return a list of all packages inside @racket[vdb-path].
}

@defproc[
 (vdb-find-packages/path [package-glob string?])
 (listof path?)
 ]{
 Return a list of all package paths that match @racket[package-glob].
}

@defproc[
 (vdb-find-packages [package-glob string?])
 (listof string?)
 ]{
 Return a list of all packages that match @racket[package-glob].
}
