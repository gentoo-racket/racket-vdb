;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     vdb/write))


@title[#:tag "vdb-write"]{Writing information}


@defmodule[vdb/write]


@defproc[
 (vdb-write [package-full-name string?] [vdb-key symbol?] [vdb-value any])
 void
 ]{
 Write a @racket[vdb-value] into a package's @racket[vdb-key] file.

 For the reference of accepted associations
 of @racket[vdb-key] to @racket[vdb-value] see the
 @seclink["vdb-information-keys"]{"Supported keys"} section.
}
