;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require (only-in typed/pmsf/depend PDepend)
         (only-in typed/pmsf/iuse PIuses)
         (only-in typed/pmsf/keywords PKeywords)
         (only-in typed/pmsf/slot PSlot)
         "contents.rkt")

(provide (all-defined-out))


(define-type VDBRawInfoHashTable
  (HashTable Symbol (Listof String)))

(define-type VDBCanonicalInfoHashValue
  (U (HashTable Symbol (Listof String))
     (Listof String)
     Boolean
     Exact-Nonnegative-Integer
     String
     PDepend
     PIuses
     PKeywords
     PSlot
     Contents))

(define-type VDBCanonicalInfoHashTable
  (HashTable Symbol VDBCanonicalInfoHashValue))
