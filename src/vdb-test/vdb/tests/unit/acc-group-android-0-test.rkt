;; This file is part of racket-vdb - Racket interface to Portage VDB.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-vdb is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-vdb is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-vdb.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit)
  (require vdb)
  (require pmsf/iuse)
  (require pmsf/keywords)
  (require pmsf/slot)

  (define acc-group-android-0-vdb-raw-info
    '#hash((BUILD_TIME . ("1666789375"))
           (CATEGORY . ("acct-group"))
           (CBUILD . ("x86_64-pc-linux-gnu"))
           (CFLAGS . ("-O3"))
           (CHOST . ("x86_64-pc-linux-gnu"))
           (CONTENTS . ("dir /usr"))
           (COUNTER . ("27863"))
           (CXXFLAGS . ("-O3"))
           (DEFINED_PHASES . ("install preinst pretend"))
           (DESCRIPTION . ("Group for dev-util/android-sdk"))
           (EAPI . ("7"))
           (FEATURES . ("assume-digests"))
           (INHERITED . ("user-info acct-group"))
           (IUSE . (""))
           (IUSE_EFFECTIVE . ("abi_x86_64"))
           (KEYWORDS . ("alpha"))
           (LDFLAGS . ("-Wl,-O1"))
           (PF . ("android-0"))
           (SIZE . ("14"))
           (SLOT . ("0"))
           (USE . ("abi_x86_64"))
           (android-0.ebuild . (""))
           (repository . ("gentoo"))))

  (define acc-group-android-0-vdb-canonical-info
    (hash 'BDEPEND
          #f
          'BINPKGMD5
          #f
          'BUILD_ID
          #f
          'BUILD_TIME
          1666789375
          'CATEGORY
          "acct-group"
          'CBUILD
          "x86_64-pc-linux-gnu"
          'CC
          #f
          'CFLAGS
          '("-O3")
          'CHOST
          "x86_64-pc-linux-gnu"
          'CONTENTS
          (hash (string->path "/usr")
                (content 'directory (string->path "/usr") #f #f #f))
          'COUNTER
          27863
          'CTARGET
          #f
          'CXX
          #f
          'CXXFLAGS
          '("-O3")
          'DEBUGBUILD
          #f
          'DEFINED_PHASES
          '("install" "preinst" "pretend")
          'DEPEND
          #f
          'DESCRIPTION
          "Group for dev-util/android-sdk"
          'EAPI
          7
          'FEATURES
          '("assume-digests")
          'HOMEPAGE
          #f
          'IDEPEND
          #f
          'INHERITED
          '("user-info" "acct-group")
          'INSTALL_MASK
          #f
          'IUSE
          (piuses '())
          'IUSE_EFFECTIVE
          '("abi_x86_64")
          'KEYWORDS
          (pkeywords (list (pkeyword 'stable "alpha" #f)))
          'LDFLAGS
          '("-Wl,-O1")
          'LICENSE
          #f
          'NEEDED
          #f
          'NEEDED.ELF.2
          #f
          'PDEPEND
          #f
          'PF
          "android-0"
          'PKGUSE
          #f
          'PROPERTIES
          #f
          'PROVIDES
          #f
          'QA_AM_MAINTAINER_MODE
          #f
          'QA_CONFIGURE_OPTIONS
          #f
          'QA_PREBUILT
          #f
          'QA_SONAME_NO_SYMLINK
          #f
          'RDEPEND
          #f
          'REQUIRES
          #f
          'RESTRICT
          #f
          'SIZE
          14
          'SLOT
          (pslot "0" #f)
          'USE
          '("abi_x86_64")
          'repository
          "gentoo"))

  (check-equal? (vdb-raw-info->canonical acc-group-android-0-vdb-raw-info)
                acc-group-android-0-vdb-canonical-info))
